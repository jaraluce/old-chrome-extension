// Only executed in URL https://www.linkedin.com/vsearch/f*
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.message === "send_message_to_first_contact_in_list") {
            var href = $("ol#results a.primary-action-button").attr("href");
            // Only supports the primary action displayed
            // in the search result. Send message for contacts
            // or connect for unknown
            if (href.startsWith("/messaging/compose")) {
                chrome.runtime.sendMessage({ "message": "send_message", "url": 'https://www.linkedin.com' + href });
            }
            if (href.startsWith("/people/invite")) {
                chrome.runtime.sendMessage({ "message": "connect", "url": 'https://www.linkedin.com' + href });
            }
        }
    }
);
