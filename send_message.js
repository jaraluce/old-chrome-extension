// Only executed in URL https://www.linkedin.com/messaging/compose*
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        // Fill the message body and send it
        if (request.message === "input_data") {
            // Fill the message
            $("textarea#compose-message").text(request.content);
            // Workaround for getting the send button activated
            $("textarea#compose-message").click();
            $("input#pillbox-input.pillbox-input").click();
            // Send message
            $("form.compose-message-form button.message-submit").click();
            // Close the window after 1.5 seconds. Needs change to somehing more "clever", like detecting
            // when the message is actaully sent
            setTimeout(function() { chrome.runtime.sendMessage({ "message": "close_window" }); }, 1500);
        }
    }
);
