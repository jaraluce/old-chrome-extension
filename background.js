function start(tab) {
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        var activeTab = tabs[0];
        chrome.tabs.sendMessage(activeTab.id, { "message": "send_message_to_first_contact_in_list" });
    });
}
chrome.browserAction.onClicked.addListener(start);

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.message === "send_message") {
            send_message(request.url);
        }
        if (request.message === "connect") {
            connect(request.url);
        }
        if (request.message === "close_window") {
            chrome.windows.remove(sender.tab.windowId);
        }
    }
);

function send_message(url) {
    chrome.windows.create({ 'url': url, focused: false, width: 10, height: 10 }, function(window) {
        chrome.tabs.onUpdated.addListener(function(tabId, info) {
            if (info.status == "complete" && tabId == window.tabs[0].id) {
                chrome.tabs.sendMessage(window.tabs[0].id, { "message": "input_data", "content": "This is an awesome auto message" });
            }
        });
    });
}

function connect(url) {
    chrome.windows.create({ 'url': url, focused: false }, function(window) {
        chrome.tabs.onUpdated.addListener(function(tabId, info) {
            if (info.status == "complete" && tabId == window.tabs[0].id) {
                chrome.tabs.sendMessage(window.tabs[0].id, { "message": "connect" });
            }
        });
    });
}
