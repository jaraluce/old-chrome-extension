// Only executed in URL https://www.linkedin.com/people/invite*
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.message === "connect") {
        	// Select "Friend" as LinkedIn relation. 
            $("ul#main-options input#IF-reason-iweReconnect").prop('checked', true);
            // Click invite button
            $("form#iwe-form input#send-invite-button").click();
            // Close window
            chrome.runtime.sendMessage({ "message": "close_window" });
        }
    }
);
